import React, { useEffect, useState  } from "react";
import { View, FlatList, Image, Text, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Feather } from "@expo/vector-icons";
import LogoImg from "../../assets/logo.png";
import api from "../../services/api";

import styles from "./styles";
function Incidents() {
  const navigation = useNavigation();
  const [incidents, setIncidents ] = useState([]);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [loading, setLoading ] = useState(false);

  async function loadingIncidents() {
     if(loading) {
       return;
     }
     if(total > 0 && incidents.length === total) {
       return;
     }
     setLoading(true);
     const response = await api.get('incidents', {
       params: {page}
     });
     setIncidents([...incidents,...response.data]);
     setTotal(response.headers['x-total-count']);
     setPage(page+1);
     setLoading(false);
  }

  useEffect(() => {
    loadingIncidents()
  }, [])

  function navigateToDetail(incident) {
    navigation.navigate("Detail", { incident });
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image source={LogoImg} />
        <Text style={styles.headerText}>
          Total de <Text style={styles.headerTextBold}>{total} casos</Text>.
        </Text>
      </View>
      <Text style={styles.title}>Bem-Vindo!</Text>
      <Text style={styles.description}>
        Escolha um dos casos abaixo e salve o dia.
      </Text>
      <FlatList
        style={styles.incidentList}
        data={incidents}
        keyExtractor={incident => String(incident.id)}
        showsVerticalScrollIndicator={false}
        onEndReached={loadingIncidents}
        onEndReachedThreshold={0.2}
        renderItem={({ item }) => (
          <View style={styles.incident}>
            <Text style={styles.incidentProperty}>ONG:</Text>
            <Text style={styles.incidentValue}>{item.name}</Text>

            <Text style={styles.incidentProperty}>CASO:</Text>
            <Text style={styles.incidentValue}>{item.title}</Text>

            <Text style={styles.incidentProperty}>Valor:</Text>
            <Text style={styles.incidentValue}>{Intl.NumberFormat('pt-BR',{ style: 'currency', currency:'BRL'}).format(item.value)}</Text>
            <TouchableOpacity
              style={styles.detailButton}
              onPress={() => navigateToDetail(item)}
            >
              <Text style={styles.detailButtonText}>Ver mais detalhes</Text>
              <Feather name="arrow-right" size={16} color="#E02041" />
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  );
}

export default Incidents;
